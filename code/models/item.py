from db import db


class ItemModel(db.Model):
    __tablename__ = 'items'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    price = db.Column(db.Float(precision=2))

    def __init__(self, item_id, name, price):
        self.id = item_id
        self.name = name
        self.price = price

    @classmethod
    def find_by_id(cls, item_id):
        return cls.query.filter_by(id=item_id).first()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    def update(self, name, price):
        self.name = name
        self.price = price
        self.save_to_db()

    def json(self):
        return {'id': self.id, 'name': self.name, 'price': self.price}
