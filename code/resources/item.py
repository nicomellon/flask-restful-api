from flask_restful import Resource
from flask import request
from models.item import ItemModel


class ItemCollection(Resource):
    def get(self):
        return {'items': [item.json() for item in ItemModel.query.all()]}

    def post(self):
        json_data = request.get_json()

        if ItemModel.find_by_id(json_data['id']):
            return f"An item with id: {json_data['id']} already exists.", 400

        item = ItemModel(json_data['id'], json_data['name'], json_data['price'])
        item.save_to_db()

        return item.json(), 201


class Item(Resource):
    def get(self, item_id):
        item = ItemModel.find_by_id(item_id)
        if item:
            return item.json(), 200

        return 'item not found', 404

    def patch(self, item_id):
        item = ItemModel.find_by_id(item_id)
        if not item:
            return f"item {item_id} not found", 404

        json_data = request.get_json()
        item.update(json_data['name'], json_data['price'])

        return item.json(), 200

    def delete(self, item_id):
        item = ItemModel.find_by_id(item_id)
        if item:
            item.delete_from_db()

        return f"deleted item {item_id}"
    
