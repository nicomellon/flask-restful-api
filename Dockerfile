# Base docker image
FROM python:3.10

# Add requirements
ADD requirements.txt .
RUN pip install -r requirements.txt

# Add source code
RUN mkdir /code
WORKDIR /code
ADD ./code .
